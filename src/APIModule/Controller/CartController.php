<?php

namespace APIModule\Controller;

use App;
use APIModule\Model\Products;
use APIModule\Model\Colors;
use APIModule\Model\Sizes;
use Bodeven\Cart\Cart;
use Bodeven\Cart\Order;
use Bodeven\Cart\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of ProductsController
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class CartController 
{

    /**
     * Send Order cart
     * 
     * @param App $app
     * @return JsonResponse
     */
    function cart(App $app) 
    {
        $products = $app['cart']->getProducts();
        $total    = $app['cart']->getTotalMount();

        $initial_data = array(
            'ci'        => null,
            'nombres'   => '',
            'apellidos' => '',
            'correo'    => ''
        );

        if ('POST' == $app['request']->getMethod()) {
            // Request data
            $data = $app['request']->request->all();

            if (!empty($data)) {

                $order = new Order($app['cart']);

                # Subject both
                $subject = 'Nuevo pedido - Horizonte';
                # Correo de administrador
                $adminMessage  = '<div style="margin:auto;position: relative;background: #FFF;border-top: 2px solid #00C0EF;margin-bottom: 20px;border-radius: 3px;width: 90%;box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);padding: 20px 30px">';
                $adminMessage .= '<h3 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Nuevo pedido</h3>';
                $adminMessage .= '<p>Ha recibido un pedido en Horizonte.</p>';
                $adminMessage .= '<h4 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Datos del pedido</h4>';
                $adminMessage .= $order->getTableProducts();
                $adminMessage .= '<h4 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Datos del cliente</h4>';
                $adminMessage .= "<p><b>Cédula de Identidad:</b> $data[ci].</p>";
                $adminMessage .= "<p><b>Nombre(s) y Apellido(s):</b> $data[nombres] $data[apellidos].</p>";
                $adminMessage .= "<p><b>Correo electrónico:</b> $data[correo]</p>";
                $adminMessage .= '</div>';

                # Correo del cliente
                $customerMessage  = '<div style="margin:auto;position: relative;background: #FFF;border-top: 2px solid #00C0EF;margin-bottom: 20px;border-radius: 3px;width: 90%;box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);padding: 20px 30px">';
                $customerMessage .= '<h3 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Nuevo pedido</h3>';
                $customerMessage .= '<p>Ha hecho un pedido en Horizonte.</p>';
                $customerMessage .= '<h4 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Datos del pedido</h4>';
                $customerMessage .= $order->getTableProducts();
                $customerMessage .= '<h4 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Datos enviados</h4>';
                $customerMessage .= "<p><b>Cédula de Identidad:</b> $data[ci].</p>";
                $customerMessage .= "<p><b>Nombre(s) y Apellido(s):</b> $data[nombres] $data[apellidos].</p>";
                $customerMessage .= "<p><b>Correo electrónico:</b> $data[correo]</p>";
                $customerMessage .= '</div>';
                
                $swiftAdminMessage = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom(array($data['correo'] => "$data[nombres] $data[apellidos]"))
                        ->setTo('tania_1019@hotmail.com')
                        ->setBcc(array('ramon.calle.88@gmail.com' => 'Ramon Serrano'))
                        ->setBody($adminMessage, 'text/html');
                
                $swiftCustomerMessage = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom(array('info@horizonte.net.ve' => "Info Horizonte"))
                        ->setTo(array($data['correo'] => "$data[nombres] $data[apellidos]"))
                        ->setBcc(array('ramon.calle.88@gmail.com' => 'Ramon Serrano'))
                        ->setBody($customerMessage, 'text/html');
                
                $adminResult = $customerResult = false;
                
                try {
                    // Custom GMail transport
                    $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                        ->setUsername('****')
                        ->setPassword('****');

                    $mailer = \Swift_Mailer::newInstance($transport);

                    $adminResult    = $mailer->send($swiftAdminMessage);
                    $customerResult = $mailer->send($swiftCustomerMessage);

                    // Response
                    $response = new JsonResponse(array(
                        'adminResult'    => $adminResult,
                        'customerResult' => $customerResult
                    ));
                } catch (\Swift_TransportException $ste) {
                    $mailer->getTransport()->stop();

                    // Response
                    $response = new JsonResponse(array(
                        'error' => $ste->getMessage()
                    ), 500);
                } catch (\Exception $ex) {
                    // Response
                    $response = new JsonResponse(array(
                        'error' => $ex->getMessage()
                    ), 500);
                }
            }
        } else {
            $response = new JsonResponse(array(
                'products' => $products,
                'total'    => $total 
            ));
        }

        return $response;
    }

    /**
     * Add product to cart
     * 
     * @param App $app
     * @return JsonResponse
     */
    function addProductToCart(App $app)
    {
        if ('POST' == $app['request']->getMethod()) {
            $productId = (int) $app['request']->request->get('productId');
            $counter   = $app['request']->request->get('counter');
            $color     = $app['request']->request->get('color');
            $size      = $app['request']->request->get('size');

            $product = new Product($productId, $app, $color, $size, $counter);

            $app['cart']->addProduct($product);

            $response = new JsonResponse(array(
                'result' => true
            ));
        }
        
        return $response;
    }

    /**
     * Plus product action
     * 
     * @param App $app
     * @return JsonResponse
     */
    function plusProduct(App $app)
    {
        if ('POST' == $app['request']->getMethod()) {
            $productId = (int) $app['request']->request->get('productId');
            $color     = $app['request']->request->get('color');
            $size      = $app['request']->request->get('size');

            $product = new Product($productId, $app, $color, $size);

            $app['cart']->addProduct($product);

            $response = new JsonResponse(array(
                'result' => true
            ));
        }
        
        return $response;
    }
    
    /**
     * Minus product action
     * 
     * @param App $app
     * @return RedirectResponse
     */
    function minusProduct(App $app)
    {
        if ('POST' == $app['request']->getMethod()) {
            $productId = (int) $app['request']->request->get('productId');
            $color     = $app['request']->request->get('color');
            $size      = $app['request']->request->get('size');

            $product = new Product($productId, $app, $color, $size);

            $app['cart']->removeProduct($productId, $color, $size);

            $response = new JsonResponse(array(
                'result' => true
            ));
        }
        
        return $response;
    }
    
    /**
     * Remove product action
     * 
     * @param App $app
     * @return RedirectResponse
     */
    function removeProduct(App $app)
    {
        if ('POST' == $app['request']->getMethod()) {
            $productId = (int) $app['request']->get('productId');
            $color     = $app['request']->get('color');
            $size      = $app['request']->get('size');

            $product = $app['cart']->getProduct($productId, $color, $size);

            if (is_object($product)) {
                for ($i = $product->count; $i >= 0; $i--) {
                    $app['cart']->removeProduct($productId, $color, $size);
                }
            }

            $response = new JsonResponse(array(
                'result' => true
            ));
        } else {
            $response = new JsonResponse(array(
                'result' => false
            ));
        }

        return new $response;
    }

}