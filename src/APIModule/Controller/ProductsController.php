<?php

namespace APIModule\Controller;

use App;
use APIModule\Model\Products;
use APIModule\Model\Colors;
use APIModule\Model\Sizes;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of ProductsController
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class ProductsController 
{

    static $LIMIT = 2;

    /**
     * Authenticate middleware
     */
    public function authenticate() 
    {
        $valid_passwords = array (
            "admin" => "admin",
        );
        $valid_users = array_keys($valid_passwords);

        $user = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '';
        $pass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';

        return (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);
    }

    /**
     * Create or Update usuarios
     */
    function restricted(App $app)
    {
        // Authenticate
        $validated = $this->authenticate();
        
        if (!$validated) {
            $response = new JsonResponse('Not Authorized', 401);
            $response->headers->set('WWW-Authenticate', 'Basic realm="My Realm"');
        } else {
            $response = new JsonResponse(array('response' => 'Authorized'));
        }
        
        return $response;
    }

	/**
     * List Products action
     * 
     * @param App $app
     * @return JsonResponse
     */
    function index(App $app)
    {
        // Limit products
        $limit = 2;
        // Limit page
        $page = $app['request']->get('page');

        if (isset($app['db'])) {
            $db = $app['db'];
        } else if (isset($app['orm.em'])) {
            $db = $app['orm.em']->getConnection();
        } else {
            throw new Exception("DB connection not found");
        }

        if ($page) {
            $numCurrentPage = ($page - 1) * $limit;
        } else {
            $page = 1;
            $numCurrentPage = 0;
        }

        $rows = Products::getInstance($db)->getAllJoined(null, "LIMIT $numCurrentPage, $limit");

        foreach ($rows as $productKey => $product) {
            $colors_sql = "SELECT * FROM `colors` WHERE product_id = ?";
            $rows[$productKey]['colors'] = $db->fetchAll($colors_sql, array($product['id']));

            $sizes_sql = "SELECT * FROM `sizes` WHERE product_id = ?";
            $rows[$productKey]['sizes'] = $db->fetchAll($sizes_sql, array($product['id']));
        }

        return new JsonResponse($rows);
    }

    /**
     * Product action
     * 
     * @param App $app
     * @param int $id
     * @return JsonResponse
     */
    function product(App $app, $id)
    {
        if (isset($app['db'])) {
            $db = $app['db'];
        } else if (isset($app['orm.em'])) {
            $db = $app['orm.em']->getConnection();
        } else {
            throw new Exception("DB connection not found");
        }

        // Colors model
        $colorModel   = Colors::getInstance($db);
        // Sizes Model
        $sizeModel    = Sizes::getInstance($db);
        // Products Model
        $productModel = Products::getInstance($db);

        $product = $productModel->getById($id);

        if(!$product) {
            $response = new JsonResponse(array(
                'error' => 'Product not found'
            ), 404);
        } else {

            $product['colors'] = $colorModel->getAll(array(), array(), "WHERE product_id = $id");
            $product['sizes']  = $sizeModel->getAll(array(), array(), "WHERE product_id = $id");

            $relatedProducts = $productModel->getAllRelated("(p.provider_id = $product[provider_id] OR p.category_id = $product[category_id]) AND p.id != $id", "LIMIT 0,5");
            
            foreach($relatedProducts as $key => $relatedProduct){
                $relatedProducts[$key]['colors'] = $colorModel->getAll(array(), array(), "WHERE product_id = $relatedProduct[id]");

                $relatedProducts[$key]['sizes'] = $sizeModel->getAll(array(), array(), "WHERE product_id = $relatedProduct[id]");
            }

            $response = new JsonResponse(array(
                'product' => $product,
                'related' => $relatedProducts
            ));
        }

        return $response;
    }

    /**
     * Limit page action
     * 
     * @param App $app
     * @return JsonResponse
     */
    function limitPage(App $app) 
    {
        if (isset($app['db'])) {
            $db = $app['db'];
        } else if (isset($app['orm.em'])) {
            $db = $app['orm.em']->getConnection();
        } else {
            throw new Exception("DB connection not found");
        }

        $rows = Products::getInstance($db)->getAll();

        $limitPages = (int) (count($rows)/self::$LIMIT);

        return new JsonResponse(array(
            'limit' => $limitPages
        ));
    }
    
    /**
     * Create action page
     * 
     * @param App $app
     * @return JsonResponse
     */
    function create(App $app)
    {
        if (isset($app['db'])) {
            $db = $app['db'];
        } else if (isset($app['orm.em'])) {
            $db = $app['orm.em']->getConnection();
        } else {
            throw new Exception("DB connection not found");
        }

    	if("POST" == $app['request']->getMethod()) {
            // Data
            $data = $app['request']->request->all();

            // Affected rows
            $affectedRows = Products::getInstance($db)->insert($data);

			return new JsonResponse(array(
                'inserted' => ($affectedRows > 0)
                ), 200);
	    }
	    
	    return new JsonResponse(array(
            'error' => 'Only method POST'
            ), 400);
    }
    
    /**
     * Edit action page
     * 
     * @param App $app
     * @param int $id
     * @return JsonResponse
     */
    function edit(App $app, $id)
    {
        if (isset($app['db'])) {
            $db = $app['db'];
        } else if (isset($app['orm.em'])) {
            $db = $app['orm.em']->getConnection();
        } else {
            throw new Exception("DB connection not found");
        }

        if ("POST" == $app['request']->getMethod() || "PUT" == $app['request']->getMethod()) {
            // Data
            $data = $app['request']->request->all();

            // Affected rows
            $affectedRows = Products::getInstance($db)->update($data);

            return new JsonResponse(array(
                'updated' => ($affectedRows > 0)
                ), 200);
        }

        return new JsonResponse(array(
            'error' => 'Only method POST|PUT allowed'
            ), 400);
    }
    
    /**
     * Delete action page
     * 
     * @param App $app
     * @param int $id
     * @return JsonResponse
     */
    function delete(App $app, $id)
    {
        if (isset($app['db'])) {
            $db = $app['db'];
        } else if (isset($app['orm.em'])) {
            $db = $app['orm.em']->getConnection();
        } else {
            throw new Exception("DB connection not found");
        }

        if("DELETE" == $app['request']->getMethod()) {
            // Affected rows
            $affectedRows = Products::getInstance($db)->delete($id);

            return new JsonResponse(array(
                'deleted' => ($affectedRows > 0)
                ), 200);
        }

        return new JsonResponse(array(
            'error' => 'Only method DELETE allowed'
            ), 400);
    }
}