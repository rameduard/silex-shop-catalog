<?php

namespace APIModule\Model;

use Application\Model\Size;
use Doctrine\DBAL\Connection;

/**
 * Description of Sizes
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class Sizes extends Size
{

	/**
     * Delete 
     * 
     * @param int $id
     * @return int
     * @throws \LogicException
     */
    public function delete($id = null)
    {
        if (!$id) {
            throw new \LogicException('Can\'t delete without param id.');
        }
        
        return $this->_delete(array('id' => $id));
    }

    /**
     * {@inheritdoc}
     */
    public function insert(array $data)
    {
        return $this->_insert($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update($id = null, array $data = array())
    {
        if (!$id) {
            throw new \LogicException('Can\'t update without param id.');
        }

        $criteria = array('id' => $id);

        return $this->_update($data, $criteria);
    }

}