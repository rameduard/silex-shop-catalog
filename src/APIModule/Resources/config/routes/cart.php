<?php

/**
 * API routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */

// Cart Info
$app->get('/api/cart/info', 'APIModule\\Controller\\CartController::cart')
    ->bind('api_cart_info');

// Add Product to cart
$app->post('/api/cart/add-product', 'APIModule\\Controller\\CartController::addProductToCart')
    ->bind('api_cart_add_product');

// Send Order
$app->post('/api/cart/send-order', 'APIModule\\Controller\\CartController::cart')
    ->bind('api_cart_send_order');

// Plus Product
$app->post('/api/cart/plus-product', 'APIModule\\Controller\\CartController::plusProduct')
    ->bind('api_cart_plus_product');

// Minus Product
$app->post('/api/cart/minus-product', 'APIModule\\Controller\\CartController::minusProduct')
    ->bind('api_cart_minus_product');

// Remove Product
$app->post('/api/cart/remove-product', 'APIModule\\Controller\\CartController::removeProduct')
    ->bind('api_cart_remove_product');