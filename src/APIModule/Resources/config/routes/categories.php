<?php

/**
 * categories API routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */


/**
 * List route
 */
$app->get('/api/categories', 'APIModule\\Controller\\CategoriesController::index')
    ->bind('api_categories_list');

/**
 * Create route
 */
$app->match('/api/categories/create', 'APIModule\\Controller\\CategoriesController::create')
    ->bind('api_categories_create');

/**
 * Edit route
 */
$app->match('/api/categories/edit/{id}', 'APIModule\\Controller\\CategoriesController::edit')
    ->bind('api_categories_edit');

/**
 * Delete route
 */
$app->match('/api/categories/delete/{id}', 'APIModule\\Controller\\CategoriesController::delete')
    ->bind('api_categories_delete');

/**
 * Restricted route
 */
$app->get('/api/categories/restricted', 'APIModule\\Controller\\CategoriesController::restricted')
    ->bind('api_categories_restricted');