<?php

/**
 * colors API routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */


/**
 * List route
 */
$app->get('/api/colors', 'APIModule\\Controller\\ColorsController::index')
    ->bind('api_colors_list');

/**
 * Create route
 */
$app->match('/api/colors/create', 'APIModule\\Controller\\ColorsController::create')
    ->bind('api_colors_create');

/**
 * Edit route
 */
$app->match('/api/colors/edit/{id}', 'APIModule\\Controller\\ColorsController::edit')
    ->bind('api_colors_edit');

/**
 * Delete route
 */
$app->match('/api/colors/delete/{id}', 'APIModule\\Controller\\ColorsController::delete')
    ->bind('api_colors_delete');

/**
 * Restricted route
 */
$app->get('/api/colors/restricted', 'APIModule\\Controller\\ColorsController::restricted')
    ->bind('api_colors_restricted');