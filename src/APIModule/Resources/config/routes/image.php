<?php

/**
 * image API routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */


/**
 * List route
 */
$app->get('/api/image', 'APIModule\\Controller\\ImageController::index')
    ->bind('api_image_list');

/**
 * Create route
 */
$app->match('/api/image/create', 'APIModule\\Controller\\ImageController::create')
    ->bind('api_image_create');

/**
 * Edit route
 */
$app->match('/api/image/edit/{id}', 'APIModule\\Controller\\ImageController::edit')
    ->bind('api_image_edit');

/**
 * Delete route
 */
$app->match('/api/image/delete/{id}', 'APIModule\\Controller\\ImageController::delete')
    ->bind('api_image_delete');

/**
 * Restricted route
 */
$app->get('/api/image/restricted', 'APIModule\\Controller\\ImageController::restricted')
    ->bind('api_image_restricted');