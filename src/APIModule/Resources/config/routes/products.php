<?php

/**
 * products API routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */


/**
 * List route
 */
$app->get('/api/products', 'APIModule\\Controller\\ProductsController::index')
    ->bind('api_products_list');

/**
 * Num prods route
 */
$app->match('/api/products/limit-page', 'APIModule\\Controller\\ProductsController::limitPage')
    ->bind('api_products_limit_page');

/**
 * Product route
 */
$app->match('/api/products/{id}', 'APIModule\\Controller\\ProductsController::product')
    ->bind('api_products_product_get');

/**
 * Create route
 */
$app->match('/api/products/create', 'APIModule\\Controller\\ProductsController::create')
    ->bind('api_products_create');

/**
 * Edit route
 */
$app->match('/api/products/edit/{id}', 'APIModule\\Controller\\ProductsController::edit')
    ->bind('api_products_edit');

/**
 * Delete route
 */
$app->match('/api/products/delete/{id}', 'APIModule\\Controller\\ProductsController::delete')
    ->bind('api_products_delete');

/**
 * Restricted route
 */
$app->get('/api/products/restricted', 'APIModule\\Controller\\ProductsController::restricted')
    ->bind('api_products_restricted');