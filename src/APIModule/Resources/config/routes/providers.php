<?php

/**
 * providers API routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */


/**
 * List route
 */
$app->get('/api/providers', 'APIModule\\Controller\\ProvidersController::index')
    ->bind('api_providers_list');

/**
 * Create route
 */
$app->match('/api/providers/create', 'APIModule\\Controller\\ProvidersController::create')
    ->bind('api_providers_create');

/**
 * Edit route
 */
$app->match('/api/providers/edit/{id}', 'APIModule\\Controller\\ProvidersController::edit')
    ->bind('api_providers_edit');

/**
 * Delete route
 */
$app->match('/api/providers/delete/{id}', 'APIModule\\Controller\\ProvidersController::delete')
    ->bind('api_providers_delete');

/**
 * Restricted route
 */
$app->get('/api/providers/restricted', 'APIModule\\Controller\\ProvidersController::restricted')
    ->bind('api_providers_restricted');