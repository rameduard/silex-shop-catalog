<?php

/**
 * sizes API routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */


/**
 * List route
 */
$app->get('/api/sizes', 'APIModule\\Controller\\SizesController::index')
    ->bind('api_sizes_list');

/**
 * Create route
 */
$app->match('/api/sizes/create', 'APIModule\\Controller\\SizesController::create')
    ->bind('api_sizes_create');

/**
 * Edit route
 */
$app->match('/api/sizes/edit/{id}', 'APIModule\\Controller\\SizesController::edit')
    ->bind('api_sizes_edit');

/**
 * Delete route
 */
$app->match('/api/sizes/delete/{id}', 'APIModule\\Controller\\SizesController::delete')
    ->bind('api_sizes_delete');

/**
 * Restricted route
 */
$app->get('/api/sizes/restricted', 'APIModule\\Controller\\SizesController::restricted')
    ->bind('api_sizes_restricted');