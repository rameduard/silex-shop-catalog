<?php

/**
 * users API routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */


/**
 * List route
 */
$app->get('/api/users', 'APIModule\\Controller\\UsersController::index')
    ->bind('api_users_list');

/**
 * Create route
 */
$app->match('/api/users/create', 'APIModule\\Controller\\UsersController::create')
    ->bind('api_users_create');

/**
 * Edit route
 */
$app->match('/api/users/edit/{id}', 'APIModule\\Controller\\UsersController::edit')
    ->bind('api_users_edit');

/**
 * Delete route
 */
$app->match('/api/users/delete/{id}', 'APIModule\\Controller\\UsersController::delete')
    ->bind('api_users_delete');

/**
 * Restricted route
 */
$app->get('/api/users/restricted', 'APIModule\\Controller\\UsersController::restricted')
    ->bind('api_users_restricted');