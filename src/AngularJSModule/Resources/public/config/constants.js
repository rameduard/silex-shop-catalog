(function () {
    'use strict';

    angular
        .module('app')
        .constant('API_URL', 'http://localhost/silex-shop-catalog/web/api');

})();
