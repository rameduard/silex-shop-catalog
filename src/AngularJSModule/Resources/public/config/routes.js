(function () {
    'use strict';

    angular
        .module('app')
        .config(AppRoutes);

    AppRoutes.$inject = ['$routeProvider'];

    function AppRoutes ($routeProvider) {
        // Home
        $routeProvider.when('/inicio', {
            templateUrl: TEMPLATES_PATH + '/home.html',
            controller: 'HomeCtrl'
        });

        // About
        $routeProvider.when('/nosotros', {
            templateUrl: TEMPLATES_PATH + '/about.html',
            controller: 'AboutCtrl'
        });

        // Catalog
        $routeProvider.when('/catalogo', {
            templateUrl: TEMPLATES_PATH + '/catalog.html',
            controller: 'CatalogCtrl'
        });

        // Catalog Item
        $routeProvider.when('/producto/:id', {
            templateUrl: TEMPLATES_PATH + '/product.html',
            controller: 'ProductCtrl'
        });

        // Cart Item
        $routeProvider.when('/carrito', {
            templateUrl: TEMPLATES_PATH + '/cart.html',
            controller: 'CartCtrl'
        });

        // Contact
        $routeProvider.when('/contactanos', {
            templateUrl: TEMPLATES_PATH + '/contact.html',
            controller: 'ContactCtrl'
        });

        // Sign In
        $routeProvider.when('/sign-in', {
            templateUrl: TEMPLATES_PATH + '/sign-in.html',
            controller: 'SignInCtrl'
        });

        // Sign Up
        $routeProvider.when('/sign-up', {
            templateUrl: TEMPLATES_PATH + '/sign-up.html',
            controller: 'SignUpCtrl'
        });

        $routeProvider.otherwise({redirectTo: '/inicio'});
    }
})();