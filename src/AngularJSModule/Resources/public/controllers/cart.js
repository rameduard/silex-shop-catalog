(function () {
    'use strict';

    angular
        .module('app')
        .controller('CartCtrl', CartCtrl);

    CartCtrl.$inject = ['$scope', 'CatalogService'];

    function CartCtrl ($scope, CatalogService) {
        $scope.products = [];
        $scope.total    = 0.0;

        // For errors and success messages
        $scope.callout = {
            class: '',
            danger: false,
            info: false,
            success: false,
            message: ''
        };

        // Show callout
        function callout(type, message) {
            $scope.callout['class']   = type;
            $scope.callout[type]      = true;
            $scope.callout['message'] = message;
        }

        $scope.plusProduct = function (productId, color, size) {
            CatalogService.plusProduct(productId, color, size, function (error, response) {
                if (error) {
                    callout('danger', error.message);
                } else {
                    _getCartInfo();
                }
            });
        };

        $scope.minusProduct = function (productId, color, size) {
            CatalogService.minusProduct(productId, color, size, function (error, response) {
                if (error) {
                    callout('danger', error.message);
                } else {
                    _getCartInfo();
                }
            });
        };

        $scope.removeProduct = function (productId, color, size) {
            CatalogService.removeProduct(productId, color, size, function (error, response) {
                if (error) {
                    callout('danger', error.message);
                } else {
                    _getCartInfo();
                }
            });
        };

        $scope.sendOrder = function () {

        };

        function _getCartInfo() {
            CatalogService.getCartInfo(function (error, products, total) {
                if (error) {
                    callout('danger', error.message);
                } else {
                    $scope.products = products;
                    $scope.total    = total;
                }
            });
        }

        _getCartInfo();

    }
})();