(function () {
    'use strict';

    angular
        .module('app')
        .controller('CatalogCtrl', CatalogCtrl);

    CatalogCtrl.$inject = ['$scope', 'CatalogService'];

    function CatalogCtrl ($scope, CatalogService) {
        $scope.currentPage = 1;
        $scope.limitPage   = 1;
        $scope.categories  = [];
        $scope.products    = [];
        $scope.providers   = [];
        
        // Filter
        $scope.filter = {
            category: '',
            provider: '',
            search: ''
        };

        // For errors and success messages
        $scope.callout = {
            class: '',
            danger: false,
            info: false,
            success: false,
            message: ''
        };

        function callout(type, message) {
            $scope.callout['class']   = type;
            $scope.callout[type]      = true;
            $scope.callout['message'] = message;
        }

        $scope.resetCallout = function () {
            $scope.callout = {
                class: '',
                danger: false,
                info: false,
                success: false,
                message: ''
            };
        }

        $scope.showAll = function () {
            $scope.filter.category = '';
            $scope.filter.provider = '';
            $scope.filter.search   = '';
        };

        $scope.setCategory = function (value) {
            $scope.filter.category = value;
        };

        $scope.setProvider = function (value) {
            $scope.filter.provider = value;
        };

        $scope.loadMore = function () {
            // If not limit page
            if ($scope.currentPage < $scope.limitPage) {
                $scope.currentPage++;

                CatalogService.getProducts($scope.currentPage, function (error, products) {
                    if (error) {
                        callout('danger', error.message);
                    } else {
                        for (var i = 0; i < products.length; i++) {
                            $scope.products.push(products[i]);
                        }
                    }
                });
            }
        };

        CatalogService.getCategories(function (error, categories) {
            if (error) {
                callout('danger', error.message);
            } else {
                $scope.categories = categories;
            }
        });

        CatalogService.getProviders(function (error, providers) {
            if (error) {
                callout('danger', error.message);
            } else {
                $scope.providers = providers;
            }
        });

        CatalogService.getProducts($scope.currentPage, function (error, products) {
            if (error) {
                callout('danger', error.message);
            } else {
                $scope.products = products;
            }
        });

        CatalogService.getLimitPage(function (error, limitPage) {
            if (error) {
                callout('danger', error.message);
            } else {
                $scope.limitPage = limitPage;
            }
        });
    }
})();