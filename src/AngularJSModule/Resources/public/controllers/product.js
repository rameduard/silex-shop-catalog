(function () {
    'use strict';

    angular
        .module('app')
        .controller('ProductCtrl', ProductCtrl);

    ProductCtrl.$inject = [
        '$scope', 
        '$routeParams', 
        'CatalogService'
    ];

    function ProductCtrl ($scope, $routeParams, CatalogService) {
        $scope.productId = $routeParams.id;
        $scope.counter   = 1;
        $scope.product   = [];
        $scope.color     = '';
        $scope.size      = '';

        // For errors and success messages
        $scope.callout = {
            class: '',
            danger: false,
            info: false,
            success: false,
            message: ''
        };

        function callout(type, message) {
            $scope.callout['class']   = type;
            $scope.callout[type]      = true;
            $scope.callout['message'] = message;
        }

        $scope.resetCallout = function () {
            $scope.callout = {
                class: '',
                danger: false,
                info: false,
                success: false,
                message: ''
            };
        }

        $scope.sumCounter = function () {
            $scope.counter++;
        };

        $scope.minusCounter = function () {
            if ($scope.counter > 1) {
                $scope.counter--;
            }
        };

        $scope.changeCounter = function () {
            if ($scope.counter <= 0) {
                $scope.counter = 1;
            }
        };

        $scope.setColor = function (value) {
            $scope.color = value;
        };

        $scope.setSize = function (value) {
            $scope.size = value;
        };

        $scope.addToCart = function () {
            // Add Product to cart
            CatalogService.addProductToCart($scope.productId, $scope.counter, $scope.color, $scope.size, function(error) {
                if (error) {
                    callout('danger', error.message);
                } else {
                    callout('success', 'Producto agregado satisfactoriamente.');
                }
            });
        };

        CatalogService.getProduct($scope.productId, function (error, product, related) {
            if (error) {
                callout('danger', error.message);
            } else {
                $scope.product = product;
                $scope.related = related;
            }
        });
    }

})();