// AngularJS App
(function () {
	'use strict';

	// Declare App Module
	angular.module('app', ['ngRoute', 'infinite-scroll'])

})();

(function ($) {
    // Admin prevent default menu
    $('.nav > li.dropdown > a').click(function(evt) {
        evt.preventDefault();
    });
})(jQuery);