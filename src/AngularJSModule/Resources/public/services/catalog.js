(function () {
    'use strict';

    angular
        .module('app')
        .factory('CatalogService', CatalogService);

    function CatalogService($http, API_URL, $window) {
        var service = {};

        function _logErrors(method, data) {
            console.log('CatalogService->' + method + ', Data: ' + JSON.stringify(data));
        }

        function _request(method, route, params, data) {
            return $http({
                method: method,
                url: API_URL + '/' + route,
                params: params,
                data: data
            })
                .catch(function (errorResp) {
                    // log error
                    _logErrors('_get' + route, errorResp['data']);
                });
        }

        function _get (route, params) {
            return _request('GET', route, params);
        }

        function _post (route, data) {
            return _request('POST', route, {}, data);
        }

        service.getCategories = function (callback) {
            _get('categories')
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Get categories failed'), errorResp['data']);
                });
        };

        service.getProducts = function (page, callback) {
            _get('products', { page: page})
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Get products failed'), errorResp['data']);
                });
        };

        service.getProduct = function (id, callback) {
            _get('products/' + id)
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']['product'], response['data']['related']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Get product failed'), errorResp['data']);
                });
        };

        service.getProviders = function (callback) {
            _get('providers')
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Get providers failed'), errorResp['data']);
                });
        };

        service.getLimitPage = function (callback) {
            _get('products/limit-page')
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']['limit']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Get limit page failed'), errorResp['data']);
                });
        };

        service.addProductToCart = function (productId, counter, color, size, callback) {
            _post('cart/add-product', {
                productId: productId,
                counter: counter,
                color: color,
                size: size
            })
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Add product failed'), errorResp['data']);
                });
        };

        service.plusProduct = function (productId, color, size, callback) {
            _post('cart/plus-product', {
                productId: productId,
                color: color,
                size: size
            })
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Sum product failed'), errorResp['data']);
                });
        };

        service.minusProduct = function (productId, color, size, callback) {
            _post('cart/minus-product', {
                productId: productId,
                color: color,
                size: size
            })
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Minus product failed'), errorResp['data']);
                });
        };

        service.removeProduct = function (productId, color, size, callback) {
            _post('cart/remove-product', {
                productId: productId,
                color: color,
                size: size
            })
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Remove product failed'), errorResp['data']);
                });
        };

        service.getCartInfo = function (callback) {
            _get('cart/info')
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']['products'], response['data']['total']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Get cart info'), errorResp['data']);
                });
        }

        service.sendOrder = function (ci, name, lastname, email, callback) {
            _post('cart/send-order')
                .then(function (response) {
                    if (response.status == 200) {
                        callback(null, response['data']['adminResult'], response['data']['customerResult']);
                    }
                })
                .catch(function (errorResp) {
                    callback(new Error('Send order error'), errorResp['data']);
                });
        };

        return service;
    }

})();