<?php

namespace DashboardModule\Controller;

use App;
use Application\Model\Category;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Description of CategoryController
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class CategoryController 
{
    
    /**
     * List categories action
     * 
     * @param App $app
     * @return string
     */
    function index(App $app)
    {
        $categories = Category::getInstance($app['db'])->getAll();

        return $app['twig']->render('categories/list.html.twig', array(
            "primary_key" => 'id',
            "rows" => $categories
        ));
    }
    
    /**
     * Create action page
     * 
     * @param App $app
     * @return RedirectResponse|string
     */
    function create(App $app)
    {
        $initialData = array('name' => '');

        $form = $app['form.factory']->createBuilder('form', $initialData)
                ->add('name', 'text', array('required' => true))
                ->getForm();

        if ("POST" == $app['request']->getMethod()) {

            $form->handleRequest($app["request"]);

            if ($form->isValid()) {
                $data = $form->getData();
                
                $result = Category::getInstance($app['db'])
                        ->setName($data['name'])
                        ->save();

                if ($result) {
                    $app['session']->getFlashBag()->add(
                        'success', 
                        array(
                            'message' => '¡Categoría creada!',
                        )
                    );
                }
                
                return $app->redirect($app['url_generator']->generate('categories_list'));
            }
        }

        return $app['twig']->render('categories/create.html.twig', array(
            "form" => $form->createView()
        ));
    }
    
    /**
     * Edit action page
     * 
     * @param App $app
     * @param int $id
     * @return RedirectResponse|string
     */
    function edit(App $app, $id)
    {
        $category = Category::getInstance($app['db'])->getById($id);

        if (!$category) {
            $app['session']->getFlashBag()->add(
                'danger', 
                array(
                    'message' => '¡Categoría no encontrada!',
                )
            );
            return $app->redirect($app['url_generator']->generate('categories_list'));
        }

        $initialData = array('name' => $category['name']);

        $form = $app['form.factory']->createBuilder('form', $initialData)
                ->add('name', 'text', array('required' => true))
                ->getForm();

        if ("POST" == $app['request']->getMethod()) {

            $form->handleRequest($app["request"]);

            if ($form->isValid()) {
                $data = $form->getData();

                $result = Category::getInstance($app['db'])
                        ->setId($id)
                        ->setName($data['name'])
                        ->save();
                
                if ($result) {
                    $app['session']->getFlashBag()->add(
                        'success', 
                        array(
                            'message' => '¡Categoría editada con éxito!',
                        )
                    );
                }

                return $app->redirect($app['url_generator']->generate('categories_edit', array("id" => $id)));
            }
        }

        return $app['twig']->render('categories/edit.html.twig', array(
            "form" => $form->createView(),
            "id" => $id
        ));
    }
    
    /**
     * Delete action page
     * 
     * @param App $app
     * @param int $id
     * @return RedirectResponse
     */
    function delete(App $app, $id)
    {
        $category = Category::getInstance($app['db'])->getById($id);

        if ($category) {
            $result = Category::getInstance($app['db'])
                        ->delete($id);
            
            if ($result) {
                $app['session']->getFlashBag()->add(
                    'success', 
                    array(
                        'message' => '¡Categoría eliminada con éxito!',
                    )
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'danger', 
                array(
                    'message' => '¡Categoría no encontrada!',
                )
            );
        }

        return $app->redirect($app['url_generator']->generate('categories_list'));
    }
    
}
