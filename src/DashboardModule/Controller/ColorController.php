<?php

namespace DashboardModule\Controller;

use App;
use Application\Model\Color;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Description of ColorController
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class ColorController 
{
    
    /**
     * List colors action page
     * 
     * @param App $app
     * @param type $productId
     * @return string
     */
    function index(App $app, $productId)
    {
        $colors = Color::getInstance($app['db'])
                ->getColorsByProductId($productId);

        return $app['twig']->render('colors/list.html.twig', array(
            "primary_key"   => 'id',
            "rows"          => $colors,
            "productId"     => $productId
        ));
    }
    
    /**
     * Create action page
     * 
     * @param App $app
     * @param int $productId
     * @return RedirectResponse|string
     */
    function create(App $app, $productId)
    {
        $initialData = array(
            'product_id' => $productId,
            'name'       => '',
            'color_hex'  => '',
        );

        $form = $app['form.factory']->createBuilder('form', $initialData)
                ->add('product_id', 'hidden', array('required' => true))
                ->add('name', 'text', array('required' => true))
                ->add('color_hex', 'text', array('required' => true))
                ->getForm();

        if ("POST" == $app['request']->getMethod()) {

            $form->handleRequest($app["request"]);

            if ($form->isValid()) {
                $data = $form->getData();

                $result = Color::getInstance($app['db'])
                        ->setProductId($data['product_id'])
                        ->setName($data['name'])
                        ->setColorHex($data['color_hex'])
                        ->save();
                
                if ($result) {
                    $app['session']->getFlashBag()->add(
                        'success', 
                        array(
                            'message' => '¡Color agregado!',
                        )
                    );
                }
                
                return $app->redirect($app['url_generator']->generate('colors_list', array('productId' => $productId)));
            }
        }

        return $app['twig']->render('colors/create.html.twig', array(
            "form" => $form->createView(),
            "productId" => $productId
        ));
    }
    
    /**
     * Edit action page
     * 
     * @param App $app
     * @param int $productId
     * @return RedirectResponse|string
     */
    function edit(App $app, $productId, $id)
    {
        $color = Color::getInstance($app['db'])->getById($id);

        if (!$color) {
            $app['session']->getFlashBag()->add(
                'danger', 
                array(
                    'message' => '¡Color del producto no encontrado!',
                )
            );
            return $app->redirect($app['url_generator']->generate('colors_list'));
        }

        $initialData = array(
            'name'      => $color['name'],
            'color_hex' => $color['color_hex'],
        );

        $form = $app['form.factory']->createBuilder('form', $initialData)
                ->add('name', 'text', array('required' => true))
                ->add('color_hex', 'text', array('required' => true))
                ->getForm();

        if ("POST" == $app['request']->getMethod()) {

            $form->handleRequest($app["request"]);

            if ($form->isValid()) {
                $data = $form->getData();

                $result = Color::getInstance($app['db'])
                        ->setId($id)
                        ->setProductId($productId)
                        ->setName($data['name'])
                        ->setColorHex($data['color_hex'])
                        ->save();
                
                if ($result) {
                    $app['session']->getFlashBag()->add(
                        'success', 
                        array(
                            'message' => '¡Color del producto editado!',
                        )
                    );
                }
                
                return $app->redirect($app['url_generator']->generate('colors_edit', array("productId" => $productId, "id" => $id)));
            }
        }

        return $app['twig']->render('colors/edit.html.twig', array(
            "form"      => $form->createView(),
            "id"        => $id,
            "productId" => $productId
        ));
    }
    
    /**
     * Delete action page
     * 
     * @param App $app
     * @param int $productId
     * @return RedirectResponse
     */
    function delete(App $app, $productId, $id)
    {
        $color = Color::getInstance($app['db'])->getById($id);

        if ($color) {
            $result = Color::getInstance($app['db'])
                    ->delete($id);
            
            if ($result) {
                $app['session']->getFlashBag()->add(
                    'success', 
                    array(
                        'message' => '¡Color del producto eliminado!',
                    )
                );
            }
        } else {
            $app['session']->getFlashBag()->add(
                'danger', 
                array(
                    'message' => '¡Color del producto no encontrado!',
                )
            );
        }

        return $app->redirect($app['url_generator']->generate('colors_list', array("productId" => $productId)));
    }
    
}
