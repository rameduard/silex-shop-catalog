<?php

namespace DashboardModule\Controller;

use App;

/**
 * Description of DashboardController
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class DashboardController 
{
    
    /**
     * Dashboard action page
     * 
     * @param App $app
     * @return string
     */
    function dashboard(App $app)
    {
        $find_sql   = "SELECT id FROM `categories`";
        $numCat = $app['db']->fetchAll($find_sql, array());

        $find_sql   = "SELECT id FROM `image`";
        $numImag = $app['db']->fetchAll($find_sql, array());

        $find_sql = "SELECT id FROM `products`";
        $numProd = $app['db']->fetchAll($find_sql, array());

        $find_sql = "SELECT id FROM `providers`";
        $numProv = $app['db']->fetchAll($find_sql, array());

        $find_sql = "SELECT id FROM `users`";
        $numUsers = $app['db']->fetchAll($find_sql, array());

        return $app['twig']->render('dashboard/dashboard.twig', array(
            "numCat"  => count($numCat),
            "numProd" => count($numProd),
            "numProv" => count($numProv),
            "numUsers" => count($numUsers),
            "numImag" => count($numImag),
        ));
    }
    
}
