<?php

namespace DashboardModule\Controller;

use App;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Description of EstatusController
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class EstatusController 
{
    
    /**
     * List action page
     * 
     * @param App $app
     * @return string
     */
    function index(App $app)
    {
        $table_columns = array(
            'id',
            'name',
        );

        $primary_key = "id";
        $rows = array();

        $find_sql = "SELECT * FROM `estatus`";
        $rows_sql = $app['db']->fetchAll($find_sql, array());

        foreach ($rows_sql as $row_key => $row_sql) {
            for ($i = 0; $i < count($table_columns); $i++) {
                $rows[$row_key][$table_columns[$i]] = $row_sql[$table_columns[$i]];
            }
        }

        return $app['twig']->render('estatus/list.html.twig', array(
            "table_columns" => $table_columns,
            "primary_key"   => $primary_key,
            "rows"          => $rows
        ));
    }
    
    /**
     * Create action page
     * 
     * @param App $app
     * @return RedirectResponse|string
     */
    function create(App $app)
    {
        $initial_data = array(
            'name' => '',
        );

        $form = $app['form.factory']->createBuilder('form', $initial_data);

        $form = $form->add('name', 'text', array('required' => true));

        $form = $form->getForm();

        if ("POST" == $app['request']->getMethod()) {

            $form->handleRequest($app["request"]);

            if ($form->isValid()) {
                $data = $form->getData();

                $update_query = "INSERT INTO `estatus` (`name`) VALUES (?)";
                $app['db']->executeUpdate($update_query, array($data['name']));

                $app['session']->getFlashBag()->add(
                    'success', 
                    array(
                        'message' => '¡Estatus creado!',
                    )
                );
                return $app->redirect($app['url_generator']->generate('estatus_list'));
            }
        }

        return $app['twig']->render('estatus/create.html.twig', array(
            "form" => $form->createView()
        ));
    }
    
    /**
     * Edit action page
     * 
     * @param App $app
     * @param type $id
     * @return RedirectResponse|string
     */
    function edit(App $app, $id)
    {
        $find_sql = "SELECT * FROM `estatus` WHERE `id` = ?";
        $row_sql = $app['db']->fetchAssoc($find_sql, array($id));

        if (!$row_sql) {
            $app['session']->getFlashBag()->add(
                'danger', 
                array(
                    'message' => '¡Estatus no encontrado!',
                )
            );
            return $app->redirect($app['url_generator']->generate('estatus_list'));
        }

        $initial_data = array(
            'name' => $row_sql['name'],
        );

        $form = $app['form.factory']->createBuilder('form', $initial_data);

        $form = $form->add('name', 'text', array('required' => true));

        $form = $form->getForm();

        if ("POST" == $app['request']->getMethod()) {

            $form->handleRequest($app["request"]);

            if ($form->isValid()) {
                $data = $form->getData();

                $update_query = "UPDATE `estatus` SET `name` = ? WHERE `id` = ?";
                $app['db']->executeUpdate($update_query, array($data['name'], $id));

                $app['session']->getFlashBag()->add(
                    'success', 
                    array(
                        'message' => '¡Estatus actualizado!',
                    )
                );
                return $app->redirect($app['url_generator']->generate('estatus_edit', array("id" => $id)));
            }
        }

        return $app['twig']->render('estatus/edit.html.twig', array(
            "form" => $form->createView(),
            "id" => $id
        ));
    }
    
    /**
     * Delete action page
     * 
     * @param App $app
     * @param int $id
     * @return RedirectResponse
     */
    function delete(App $app, $id)
    {
        $find_sql = "SELECT * FROM `estatus` WHERE `id` = ?";
        $row_sql = $app['db']->fetchAssoc($find_sql, array($id));

        if ($row_sql) {
            $delete_query = "DELETE FROM `estatus` WHERE `id` = ?";
            $app['db']->executeUpdate($delete_query, array($id));

            $app['session']->getFlashBag()->add(
                'success', 
                array(
                    'message' => '¡Estatus eliminado!',
                )
            );
        } else {
            $app['session']->getFlashBag()->add(
                'danger', 
                array(
                    'message' => '¡Estatus no encontrado!',
                )
            );
        }
        return $app->redirect($app['url_generator']->generate('estatus_list'));
    }
    
}
