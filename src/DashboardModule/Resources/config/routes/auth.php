<?php

/**
 * Auth routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */

$app->get('/login', 'DashboardModule\\Controller\\AuthController::login')
    ->bind('login');