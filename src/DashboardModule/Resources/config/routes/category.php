<?php

/**
 * Category routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */

$app->match('/admin/categories', 'DashboardModule\\Controller\\CategoryController::index')
    ->bind('categories_list');

$app->match('/admin/categories/create', 'DashboardModule\\Controller\\CategoryController::create')
    ->bind('categories_create');

$app->match('/admin/categories/edit/{id}', 'DashboardModule\\Controller\\CategoryController::edit')
    ->bind('categories_edit');

$app->match('/admin/categories/delete/{id}', 'DashboardModule\\Controller\\CategoryController::delete')
    ->bind('categories_delete');
