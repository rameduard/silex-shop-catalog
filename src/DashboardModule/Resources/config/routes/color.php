<?php

/**
 * Color routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */

$app->match('/admin/colors/{productId}', 'DashboardModule\\Controller\\ColorController::index')
    ->bind('colors_list');

$app->match('/admin/colors/{productId}/create', 'DashboardModule\\Controller\\ColorController::create')
    ->bind('colors_create');

$app->match('/admin/colors/{productId}/edit/{id}', 'DashboardModule\\Controller\\ColorController::edit')
    ->bind('colors_edit');

$app->match('/admin/colors/{productId}/delete/{id}', 'DashboardModule\\Controller\\ColorController::delete')
    ->bind('colors_delete');
