<?php

/**
 * Dashboard routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */

$app->match('/admin', 'DashboardModule\\Controller\\DashboardController::dashboard')
    ->bind('dashboard');
