<?php

/**
 * Estatus routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */

$app->match('/admin/estatus', 'DashboardModule\\Controller\\EstatusController::index')
    ->bind('estatus_list');

$app->match('/admin/estatus/create', 'DashboardModule\\Controller\\EstatusController::create')
    ->bind('estatus_create');

$app->match('/admin/estatus/edit/{id}', 'DashboardModule\\Controller\\EstatusController::edit')
    ->bind('estatus_edit');

$app->match('/admin/estatus/delete/{id}', 'DashboardModule\\Controller\\EstatusController::delete')
    ->bind('estatus_delete');
