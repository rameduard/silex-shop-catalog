<?php

/**
 * Image routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */

$app->match('/admin/imagen', 'DashboardModule\\Controller\\ImageController::index')
    ->bind('imagen_list');

$app->match('/admin/imagenJson', 'DashboardModule\\Controller\\ImageController::listJson')
    ->method('GET')
    ->bind('imagen_list_json');

$app->match('/admin/imagen/create', 'DashboardModule\\Controller\\ImageController::create')
    ->bind('imagen_create');

$app->match('/admin/imagen/edit/{id}', 'DashboardModule\\Controller\\ImageController::edit')
    ->bind('imagen_edit');

$app->match('/admin/imagen/delete/{id}', 'DashboardModule\\Controller\\ImageController::delete')
    ->bind('imagen_delete');
