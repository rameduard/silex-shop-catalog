$(function() {
    $(".textarea").wysihtml5();
    $('a').click(function() {
        Pace.restart();
    });
    $('form').submit(function(event) {
        Pace.restart();
    });
});