<?php

namespace FrontendModule\Controller;

use App;
use Bodeven\Cart\Cart;
use Bodeven\Cart\Order;
use Bodeven\Cart\Product;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of CartController
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class CartController 
{
    
    /**
     * Add product to cart
     * 
     * @param App $app
     * @return RedirectResponse
     */
    function addToCart(App $app)
    {
        $productId = (int) $app['request']->get('product_id');
        $count     = $app['request']->get('count');
        $color     = $app['request']->get('color');
        $size      = $app['request']->get('size');

        $product = new Product($productId, $app, $color, $size, $count);

        $app['cart']->addProduct($product);

        $app['session']->getFlashBag()->add(
            'info',
            array(
                'message' => '¡Producto agregado al pedido!',
            )
        );

        return $app->redirect($app['url_generator']->generate('catalog_item', array("id" => $productId)));
    }
    
    /**
     * Cart action page
     * 
     * @param App $app
     * @return RedirectResponse|Response
     */
    function cart(App $app)
    {
        $products    = $app['cart']->getProducts();
        $total_mount = $app['cart']->getTotalMount();

        $initial_data = array(
            'ci'        => null,
            'nombres'   => '',
            'apellidos' => '',
            'correo'    => ''
        );

        $formOrder = $app['form.factory']->createBuilder('form', $initial_data)
                    ->add('ci', 'number', array('required' => true))
                    ->add('nombres', 'text', array('required' => true))
                    ->add('apellidos', 'text', array('required' => true))
                    ->add('correo', 'email', array('required' => true))
                    ->getForm();

        if ('POST' == $app['request']->getMethod()) {

            $formOrder->handleRequest($app['request']);

            if ($formOrder->isValid()) {
                $data = $formOrder->getData();

                $order = new Order($app['cart']);

                # Subject both
                $subject = 'Nuevo pedido - Horizonte';
                # Correo de administrador
                $adminMessage  = '<div style="margin:auto;position: relative;background: #FFF;border-top: 2px solid #00C0EF;margin-bottom: 20px;border-radius: 3px;width: 90%;box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);padding: 20px 30px">';
                $adminMessage .= '<h3 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Nuevo pedido</h3>';
                $adminMessage .= '<p>Ha recibido un pedido en Horizonte.</p>';
                $adminMessage .= '<h4 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Datos del pedido</h4>';
                $adminMessage .= $order->getTableProducts();
                $adminMessage .= '<h4 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Datos del cliente</h4>';
                $adminMessage .= "<p><b>Cédula de Identidad:</b> $data[ci].</p>";
                $adminMessage .= "<p><b>Nombre(s) y Apellido(s):</b> $data[nombres] $data[apellidos].</p>";
                $adminMessage .= "<p><b>Correo electrónico:</b> $data[correo]</p>";
                $adminMessage .= '</div>';

                # Correo del cliente
                $customerMessage  = '<div style="margin:auto;position: relative;background: #FFF;border-top: 2px solid #00C0EF;margin-bottom: 20px;border-radius: 3px;width: 90%;box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);padding: 20px 30px">';
                $customerMessage .= '<h3 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Nuevo pedido</h3>';
                $customerMessage .= '<p>Ha hecho un pedido en Horizonte.</p>';
                $customerMessage .= '<h4 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Datos del pedido</h4>';
                $customerMessage .= $order->getTableProducts();
                $customerMessage .= '<h4 style="padding-bottom: 9px;border-bottom: 1px solid #EEE;">Datos enviados</h4>';
                $customerMessage .= "<p><b>Cédula de Identidad:</b> $data[ci].</p>";
                $customerMessage .= "<p><b>Nombre(s) y Apellido(s):</b> $data[nombres] $data[apellidos].</p>";
                $customerMessage .= "<p><b>Correo electrónico:</b> $data[correo]</p>";
                $customerMessage .= '</div>';
                
                $swiftAdminMessage = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom(array($data['correo'] => "$data[nombres] $data[apellidos]"))
                        ->setTo('tania_1019@hotmail.com')
                        ->setBcc(array('ramon.calle.88@gmail.com' => 'Ramon Serrano'))
                        ->setBody($adminMessage, 'text/html');
                
                $swiftCustomerMessage = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom(array('info@horizonte.net.ve' => "Info Horizonte"))
                        ->setTo(array($data['correo'] => "$data[nombres] $data[apellidos]"))
                        ->setBcc(array('ramon.calle.88@gmail.com' => 'Ramon Serrano'))
                        ->setBody($customerMessage, 'text/html');
                
                $adminResult = $customerResult = false;
                
                try {
                    $adminResult    = $app['mailer']->send($swiftAdminMessage);
                    $customerResult = $app['mailer']->send($swiftCustomerMessage);
                } catch (\Swift_TransportException $ste) {
                    $app['mailer']->getTransport()->stop();
                    throw $ste;
                }

                if ($adminResult && $customerResult) {
                    $app['cart'] = new Cart();

                    $app['session']->getFlashBag()->add(
                        'success', 
                        array(
                            'message' => '¡Su pedido fue enviado con éxito!',
                        )
                    );
                } else {
                    $app['session']->getFlashBag()->add(
                        'danger', 
                        array(
                            'message' => '¡Su pedido no pudo ser enviado! Ocurrió un error al enviar el correo.',
                        )
                    );
                }
                return $app->redirect($app['url_generator']->generate('cart'));
            }
        }

        return $app['twig']->render('cart/cart.html.twig', array(
            'products'    => $products,
            'total_mount' => $total_mount,
            'form'        => $formOrder->createView()
        ));
    }
    
    /**
     * Plus product action
     * 
     * @param App $app
     * @return RedirectResponse
     */
    function plusProduct(App $app)
    {
        $productId = $app['request']->get('product_id');
        $color     = $app['request']->get('color');
        $size      = $app['request']->get('size');

        $product = new Product($productId, $app, $color, $size);

        $app['cart']->addProduct($product);

        return $app->redirect($app['url_generator']->generate('cart'));
    }
    
    /**
     * Minus product action
     * 
     * @param App $app
     * @return RedirectResponse
     */
    function minusProduct(App $app)
    {
        $productId = $app['request']->get('product_id');
        $color     = $app['request']->get('color');
        $size      = $app['request']->get('size');

        $app['cart']->removeProduct($productId, $color, $size);

        return $app->redirect($app['url_generator']->generate('cart'));
    }
    
    /**
     * Remove product action
     * 
     * @param App $app
     * @return RedirectResponse
     */
    function removeProduct(App $app)
    {
        $productId = $app['request']->get('product_id');
        $color     = $app['request']->get('color');
        $size      = $app['request']->get('size');

        $product = $app['cart']->getProduct($productId, $color, $size);

        if (is_object($product)) {
            for ($i = $product->count; $i >= 0; $i--) {
                $app['cart']->removeProduct($productId, $color, $size);
            }
        }

        $app['session']->getFlashBag()->add(
            'success',
            array(
                'message' => '¡Producto removido del pedido!',
            )
        );

        return $app->redirect($app['url_generator']->generate('cart'));
    }
}
