<?php

namespace FrontendModule\Controller;

use App;
use Application\Model\Category;
use Application\Model\Color;
use Application\Model\Product;
use Application\Model\Provider;
use Application\Model\Size;

/**
 * Description of CatalogController
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class CatalogController 
{
    
    /**
     * Catalog action page
     * 
     * @param App $app
     * @return string
     */
    function catalog(App $app)
    {
        $categories = Category::getInstance($app['db'])->getAll(array('id', 'name'));
        
        $providers  = Provider::getInstance($app['db'])->getAll(array('id', 'name'));

        $categoryId = $app['request']->get('category');
        $providerId = $app['request']->get('provider');
        
        $productModel = Product::getInstance($app['db']);

        if ($categoryId) {
            $productsToPage = $productModel->getAll(array(), array(), "WHERE category_id = $categoryId");
        } elseif ($providerId) {
            $productsToPage = $productModel->getAll(array(), array(), "WHERE provider_id = $providerId");
        } else {
            $productsToPage = $productModel->getAll();
        }

        /**
         * Paginacion
         */
        $numProd = count($productsToPage);
        $pages = array();

        $lastPage = null;

        if ($numProd > 9) {
            $numPages = (int) ($numProd/9);

            if ($numPages > 9) {
                for ($i = 1; $i <= $numPages; $i++) {
                    $pages[] = $i;
                }
            }

            for ($i = 1; $i <= $numPages; $i++) {
                $pages[] = $i;
            }
            $lastPage = $i;
        }

        $numPage = $app['request']->get('page');

        if ($numPage) {
            $numPageActual = ($numPage - 1) * 9;
        } else {
            $numPage = 1;
            $numPageActual = 0;
        }

        /**
         * Fin de paginacion
         */
        
        $productFields = array(
            'id',
            'provider_id',
            'category_id',
            'category',
            'provider',
            'name',
            'price',
            'image',
        );

        $primary_key = "id";
        $products = array();
        
        if ($categoryId) {
            $products = $productModel->getAllJoined("p.category_id = $categoryId", "LIMIT $numPageActual, 9");
        } elseif ($providerId) {
            $products = $productModel->getAllJoined("p.provider_id = $providerId", "LIMIT $numPageActual, 9");
        } else {
            $products = $productModel->getAllJoined(null, "LIMIT $numPageActual, 9");
        }
        
        foreach($products as $productKey => $product){
            for($i = 0; $i < count($productFields); $i++){
                $products[$productKey][$productFields[$i]] = $product[$productFields[$i]];
            }
            $colors_sql = "SELECT * FROM `colors` WHERE product_id = ?";
            $products[$productKey]['colors'] = $app['db']->fetchAll($colors_sql, array($product['id']));

            $sizes_sql = "SELECT * FROM `sizes` WHERE product_id = ?";
            $products[$productKey]['sizes'] = $app['db']->fetchAll($sizes_sql, array($product['id']));
        }

        return $app['twig']->render('catalog/catalog.html.twig', array(
            "primary_key" => $primary_key,
            "categories"  => $categories,
            "products"    => $products,
            "providers"   => $providers,
            "pages"       => $pages,
            "numPage"     => $numPage,
            "lastPage"    => $lastPage,
            "category_id" => $categoryId,
            "provider_id" => $providerId
        ));
    }
    
    /**
     * Catalog Item page
     * 
     * @param App $app
     * @param int $id
     * @return type
     */
    function catalogItem(App $app, $id)
    {
        $colorModel   = Color::getInstance($app['db']);
        $sizeModel    = Size::getInstance($app['db']);
        $productModel = Product::getInstance($app['db']);
        
        $productArray = $productModel->getById($id);

        if(!$productArray) {
            return $app->redirect($app['url_generator']->generate('catalog'));
        }

        $colorsFields = array(
            'id',
            'name',
            'product_id',
            'color_hex'
        );

        $colors = $colorModel->getAll($colorsFields, array(), "WHERE product_id = $id");
        $sizes  = $sizeModel->getAll(array(), array(), "WHERE product_id = $id");

        $productArray['colors'] = $colors;
        $productArray['sizes'] = $sizes;

        $product_columns = array(
            'id',
            'provider_id',
            'category_id',
            'category',
            'provider',
            'name',
            'price',
            'image',
        );
        
        $relatedProducts = $productModel->getAllRelated("(p.provider_id = $productArray[provider_id] OR p.category_id = $productArray[category_id]) AND p.id != $id", "LIMIT 0,5");
        
        foreach($relatedProducts as $key => $relatedProduct){
            $relatedProducts[$key]['colors'] = $colorModel->getAll(array(), array(), "WHERE product_id = $relatedProduct[id]");

            $relatedProducts[$key]['sizes'] = $sizeModel->getAll(array(), array(), "WHERE product_id = $relatedProduct[id]");
        }

        return $app['twig']->render('catalog/catalog_item.html.twig', array(
            'product' => $productArray,
            'related_products' => $relatedProducts
        ));
    }
}
