<?php

/**
 * Cart routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */

$app->match('/addToCart', 'FrontendModule\\Controller\\CartController::addToCart')
    ->method('POST')
    ->bind('addToCart');

$app->match('/pedido', 'FrontendModule\\Controller\\CartController::cart')
    ->method('GET|POST')
    ->bind('cart');

$app->match('/product_plus', 'FrontendModule\\Controller\\CartController::plusProduct')
    ->method('POST')
    ->bind('product_plus');

$app->match('/product_minus', 'FrontendModule\\Controller\\CartController::minusProduct')
    ->method('POST')
    ->bind('product_minus');

$app->match('/product_remove', 'FrontendModule\\Controller\\CartController::removeProduct')
    ->method('POST')
    ->bind('product_remove');
