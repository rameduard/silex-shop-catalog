<?php

/**
 * Catalog routes
 *
 * @author Ramon Serrano <ramon.calle.88@gmail.com>
 */

$app->match('/catalog', 'FrontendModule\\Controller\\CatalogController::catalog')
    ->method('GET')
    ->bind('catalog');

$app->match('/catalog/{id}', 'FrontendModule\\Controller\\CatalogController::catalogItem')
    ->method('GET')
    ->bind('catalog_item');
