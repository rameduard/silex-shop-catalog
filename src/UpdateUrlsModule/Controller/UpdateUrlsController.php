<?php

namespace UpdateUrlsModule\Controller;

use App;
use UpdateUrlsModule\Model\Image;
use UpdateUrlsModule\Model\Product;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of UpdateUrlsModule
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class UpdateUrlsController
{

    /**
     * Index action update urls
     * 
     * @param App $app
     * @return string
     */
    function index(App $app)
    {
        $initial_data = array(
            'old_url' => '',
            'new_url' => ''
        );
        
        $form = $app['form.factory']->createBuilder('form', $initial_data)
                ->add('old_url', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank,
                        new Assert\Url(),
                    ),
                    'required' => true
                ))
                ->add('new_url', 'text', array(
                    'constraints' => array(
                        new Assert\NotBlank,
                        new Assert\Url(),
                    ),
                    'required' => true
                ))
                ->getForm();
        
        if("POST" == $app['request']->getMethod()) {
            $form->handleRequest($app["request"]);
            
            if ($form->isValid()) {
                $data = $form->getData();
                
                $productModel = new Product($app['db']);
                
                $productsReplaced = $productModel->replaceOldUrlForNewUrl($data['old_url'], $data['new_url']);
                
                $imageModel = new Image($app['db']);
                
                $imagesReplaced = $imageModel->replaceOldUrlForNewUrl($data['old_url'], $data['new_url']);
                
                $app['session']->getFlashBag()->add(
                    'success',
                    array(
                        'message' => "¡Urls actualizadas! Productos: $productsReplaced e Imágenes: $imagesReplaced.",
                    )
                );
                
                return $app->redirect($app['url_generator']->generate('update_urls_index'));
            }
        }
        
        return $app['twig']->render('update_urls/index.twig', array(
            'form' => $form->createView()
        ));
    }

}
