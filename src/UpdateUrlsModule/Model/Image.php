<?php

namespace UpdateUrlsModule\Model;

use Doctrine\DBAL\Connection;
use Application\Model\Image as BaseImage;

/**
 * Description of Product
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class Image extends BaseImage
{
    
    /**
     * Product construct
     * 
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        parent::__construct($connection);
    }
    
    /**
     * Replace Image old url for new url
     * 
     * @param string $oldUrl
     * @param string $newUrl
     * @return int
     */
    function replaceOldUrlForNewUrl($oldUrl, $newUrl)
    {
        $images = $this->getAll(array('id', 'link'), array(), "WHERE link != ''");
        $imagesReplaced = 0;
        
        foreach ($images as $image) {
            if ($link = str_replace($oldUrl, $newUrl, $image['link'])) {
                if ($this->updateUrl($image['id'], $link) > 0) {
                    $imagesReplaced++;
                }
            }
        }
        
        return $imagesReplaced;
    }
    
    /**
     * Update Image URL of product
     * 
     * @param int $id
     * @param string $newLink
     * @return int
     */
    public function updateUrl($id, $newLink)
    {
        $data = array(
            'link' => $newLink
        );
        return $this->_update($data, array('id' => $id));
    }
    
}
