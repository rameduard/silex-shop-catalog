<?php

namespace UpdateUrlsModule\Model;

use Doctrine\DBAL\Connection;
use Application\Model\Product as BaseProduct;

/**
 * Description of Product
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class Product extends BaseProduct
{
    
    /**
     * Product construct
     * 
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        parent::__construct($connection);
    }
    
    /**
     * Replace Image old url for new url
     * 
     * @param string $oldUrl
     * @param string $newUrl
     * @return int
     */
    function replaceOldUrlForNewUrl($oldUrl, $newUrl)
    {
        $products = $this->getAll(array('id', 'image'), array(), "WHERE image != ''");
        $productsReplaced = 0;
        
        foreach ($products as $product) {
            if ($image = str_replace($oldUrl, $newUrl, $product['image'])) {
                if ($this->updateUrl($product['id'], $image)) {
                    $productsReplaced++;
                }
            }
        }
        
        return $productsReplaced;
    }
    
    /**
     * Update Image URL of product
     * 
     * @param int $id
     * @param string $imageUrl
     * @return int
     */
    public function updateUrl($id, $imageUrl)
    {
        $data = array(
            'image' => $imageUrl
        );
        return $this->_update($data, array('id' => $id));
    }
    
}
