<?php

/**
 * Description of routes
 *
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */

$rootRoute = '/admin/update-urls';

$app->match($rootRoute, 'UpdateUrlsModule\\Controller\\UpdateUrlsController::index')
    ->bind('update_urls_index');
